import React from 'react'

const HOOK_SVG = 'M13 31c-6.627 0-12-5.373-12-12 0-5.497 8.25-15.627 11.066-18.926l-.066-.074s-12 13.373-12 20 5.373 12 12 12c3.568 0 6.764-1.566 8.961-4.039-2.119 1.885-4.901 3.039-7.961 3.039z'
const HOOK_SVG2 = 'M16.994 16c-1.108 0-2.006.898-2.006 2.007 0 1.107.897 2.005 2.006 2.005 1.107 0 2.006-.897 2.006-2.005 0-1.109-.898-2.007-2.006-2.007z'
const HOOK_SVG3 = 'M12 0s12 13.373 12 20-5.372 12-12 12c-6.627 0-12-5.373-12-12l12-20z'
const SPLASH = 'm105.5,400.4375c0,0 8,-98 -37,-83c-45,15 -48,-40 -48.5,-40.4375c0.5,0.4375 -11.5,-72.5625 23.5,-46.5625c35,26 85,143 83,58c-2,-85 -48,-85 -48.5,-85.4375c0.5,0.4375 -57.5,-17.5625 -10.5,-39.5625c47,-22 48,19 47.5,18.5625c0.5,0.4375 34.5,179.4375 75.5,142.4375c41,-37 32,-92 32,-93c0,-1 -47,-3 -34,-35c13,-32 39,-23 38.5,-23.4375c0.5,0.4375 37.5,5.4375 41.5,21.4375c4,16 -15,22 -17,31c-2,9 -93,176 9,139c102,-37 125,-137 124.5,-137.4375c0.5,0.4375 -26.5,-33.5625 -27,-34c0.5,0.4375 -30.5,-45.5625 31.5,-31.5625c62,14 60,70 59.5,69.5625c8.5,66.4375 -23.5,70.4375 -28.5,112.4375c-5,42 103,53 96.5,-5.4375c15.5,4.4375 25.5,-48.5625 -7.5,-106.5625c-33,-58 4,-57 3.5,-57.4375c0.5,0.4375 75.5,-16.5625 58.5,16.4375c-17,33 -25,139 -25.5,138.5625c0.5,0.4375 -4.5,82.4375 13.5,49.4375c18,-33 14,22 13.5,21.5625c0.5,0.4375 -467.5,0.4375 -467.5,0.4375z'
const HOOK_PATH = new Path2D(HOOK_SVG)
const HOOK_PATH2 = new Path2D(HOOK_SVG2)
const HOOK_PATH3 = new Path2D(HOOK_SVG3)
const SPLASH_PATH = new Path2D(SPLASH)
const SCALE = 0.8
const OFFSET = 80
const GROUND = 210

function draw(ctx, location) {
    ctx.shadowColor = 'dodgerblue'
    ctx.shadowBlur = 2
    ctx.save()
    ctx.translate(location.x / SCALE - OFFSET, location.y / SCALE - OFFSET)
    if (location.hasOwnProperty('splash')) {
        ctx.scale(0.2, 0.2)
        ctx.fillStyle = '#369DC0'
        ctx.fill(SPLASH_PATH)
    } else {
        ctx.scale(SCALE, SCALE)
        ctx.fillStyle = '#369DC0'
        ctx.fill(HOOK_PATH)
        ctx.fillStyle = '#63BFDE'
        ctx.fill(HOOK_PATH2)
        ctx.fillStyle = '#3CAED6'
        ctx.fill(HOOK_PATH3)
    }

    ctx.restore()
}

const App = () => {
    const requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

    const [locations, setLocations] = React.useState([])
    const canvasRef = React.useRef(null)

    const render = () => {
        let rnd = Math.random()
        let currlocations = locations
        if (rnd < 0.1) {
            currlocations = handleCanvasClick()
        }

        const canvas = canvasRef.current
        const ctx = canvas.getContext('2d')
        ctx.clearRect(0, 0, window.innerWidth, window.innerHeight)
        const newLocations = currlocations.map((location) => {
                let upLocation = {}
                if (location.hasOwnProperty('splash')) {
                    upLocation['x'] = location.x
                    upLocation['y'] = location.y
                    upLocation['vel'] = {x: 0, y: 0}
                    upLocation['splash'] = location.splash + 1
                } else if ((location.y) > (window.innerHeight - GROUND)) {
                    upLocation['x'] = location.x - 40
                    upLocation['y'] = location.y
                    upLocation['vel'] = {x: 0, y: 0}
                    upLocation['splash'] = 0
                } else {
                    upLocation['x'] = location.x + location.vel.x
                    upLocation['y'] = location.y + location.vel.y
                    upLocation['vel'] = location.vel
                }

                return upLocation
            }
        ).filter(e => (e.y < window.innerHeight && (!e.hasOwnProperty('splash') || e.splash < 10)))

        newLocations.forEach(location => draw(ctx, location))
        setLocations(newLocations)
    }

    requestAnimationFrame(render)
    const handleCanvasClick = (e) => {
        const newLocation = {
            x: Math.random() * window.innerWidth,
            y: 0,
            vel: {x: 0, y: Math.random() * 10}
        }
        return ([...locations, newLocation])
    }

    const handleClear = () => {
        setLocations([])
    }

    const handleUndo = () => {
        setLocations(locations.slice(0, -1))
    }

    return (
        <>
            <div className="controls">
                <button onClick={handleClear}>Clear</button>
                <button onClick={handleUndo}>Undo</button>
            </div>
            <canvas
                ref={canvasRef}
                width={window.innerWidth}
                height={window.innerHeight}
                onClick={handleCanvasClick}
            />
        </>
    )
}

export default App